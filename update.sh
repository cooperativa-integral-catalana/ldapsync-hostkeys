#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

git fetch --all &> /dev/null
git reset --hard origin/master &> /dev/null

exit 0

